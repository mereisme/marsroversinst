package main.java.domain;

public class Plateau {
    private int upperXCoord, upperYCoord;

    public Plateau(int upperXCoord, int upperYCoord) {
        this.upperXCoord = upperXCoord;
        this.upperYCoord = upperYCoord;
    }

    public int getUpperXCoord() {
        return upperXCoord;
    }

    public void setUpperXCoord(int upperXCoord) {
        this.upperXCoord = upperXCoord;
    }

    public int getUpperYCoord() {
        return upperYCoord;
    }

    public void setUpperYCoord(int upperYCoord) {
        this.upperYCoord = upperYCoord;
    }
}
