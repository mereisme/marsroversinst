package main.java.domain;

import java.util.ArrayList;

public class Rover {
    private int xLoc, yLoc;
    private char dir;
    private ArrayList<Character> instructions;

    public Rover(int xLoc, int yLoc, char dir, ArrayList instructions) {
        this.xLoc = xLoc;
        this.yLoc = yLoc;
        this.dir = dir;
        this.instructions = instructions;
    }

    public int getXLoc() {
        return xLoc;
    }

    public void setXLoc(int xLoc) {
        this.xLoc = xLoc;
    }

    public int getYLoc() {
        return yLoc;
    }

    public void setYLoc(int yLoc) {
        this.yLoc = yLoc;
    }

    public char getDir() {
        return dir;
    }

    public void setDir(char dir) {
        this.dir = dir;
    }

    public ArrayList<Character> getInstructions() {
        return instructions;
    }

    public void setInstructions(ArrayList<Character> instructions) {
        this.instructions = instructions;
    }

    public String toString() {
        return xLoc + " " + yLoc + " " + dir;
    }
}
