package main.java;

import main.java.domain.Plateau;
import main.java.domain.Rover;
import main.java.functionality.GetPlateauDetails;
import main.java.functionality.GetRoversDetails;
import main.java.functionality.ProcessRoverInstructions;

import java.util.ArrayList;

public class MarsRovers {
    public static void main(String[] args) {
        Plateau plateau = GetPlateauDetails.getPlateau();
        ArrayList<Rover> rovers = GetRoversDetails.getRovers(plateau);

        ProcessRoverInstructions.processInstructions(plateau, rovers);

        for(Rover r : rovers) {
            System.out.println(r.toString());
        }
    }
}
