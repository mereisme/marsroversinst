package main.java.functionality;

import main.java.domain.Plateau;
import main.java.domain.Rover;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class GetRoversDetails {
    public static ArrayList<Rover> getRovers(Plateau plateau) {
        ArrayList<Rover> rovers = new ArrayList<>();
        String input;
        int x = 0, y = 0;
        char dir = ' ';
        ArrayList<Character> instructions = new ArrayList<>();
        boolean validInput = false;
        Scanner in = new Scanner(System.in);
        do {
            // get location
            while (!validInput) {
                System.out.println("Input the rover's x and y location and direction separated by a space (X Y N/S/E/W)");
                input = in.nextLine();
                String[] loc = input.split(" ");
                if (loc.length == 3 && ValidationHelper.validateCoord(loc[0]) && ValidationHelper.validateCoord(loc[1])
                        && ValidationHelper.validateDir(loc[2].toUpperCase())) {
                    x = Integer.parseInt(loc[0]);
                    y = Integer.parseInt(loc[1]);
                    dir = loc[2].toUpperCase().charAt(0);
                    validInput = x <= plateau.getUpperXCoord() && y <= plateau.getUpperYCoord();
                }

                if (!validInput) {
                    System.out.println("Invalid input. Two positive integer values and a letter representing the cardinal direction must be provided, separated by spaces.");
                }
            }

            validInput = false; //reset to read next line

            // get instructions
            while (!validInput) {
                System.out.println("Input the rover's instructions (M-Move, L-Left, R-Right)");
                input = in.nextLine();
                input.replaceAll(" ", "");
                validInput = StringUtils.containsOnly(input.toUpperCase(), "MLR");

                if (!validInput) {
                    System.out.println("Invalid input. Only M, L, and R are valid instructions.");
                } else {
                    instructions = new ArrayList<Character>(Arrays.asList(ArrayUtils.toObject(input.toUpperCase().toCharArray())));
                }
            }

            rovers.add(new Rover(x, y, dir, instructions));

            // check for more rovers
            validInput = false; //reset for additional rover entry
            System.out.println("Rover successfully added. Enter 'Y' to add another rover or any key to stop.");
            input = in.nextLine();
        } while (input.equalsIgnoreCase("Y"));

        return rovers;
    }
}
