package main.java.functionality;

import org.apache.commons.lang3.StringUtils;

public class ValidationHelper {
    public static boolean validateCoord(String coord){
        return StringUtils.isNumeric(coord) && StringUtils.containsNone(coord, '-', '.');
    }

    public static boolean validateDir(String dir) {
        return dir.length() == 1 && StringUtils.containsOnly(dir, "NSEW");
    }
}
