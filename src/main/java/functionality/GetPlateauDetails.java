package main.java.functionality;

import main.java.domain.Plateau;

import java.util.Scanner;

public class GetPlateauDetails {
    public static Plateau getPlateau(){
        String input;
        int x = 0, y = 0;
        boolean validInput = false;
        Scanner in = new Scanner(System.in);
        while(!validInput) {
            System.out.println("Input the upper right bound x and y coordinates of the plateau separated by a space (X Y)");
            input = in.nextLine();
            String[] coords = input.split(" ");
            if(coords.length == 2 && ValidationHelper.validateCoord(coords[0]) && ValidationHelper.validateCoord(coords[1])) {
                x = Integer.parseInt(coords[0]);
                y = Integer.parseInt(coords[1]);
                validInput = x > 0 && y > 0;
            }

            if(!validInput) {
                System.out.println("Invalid input. Two positive integer values must be provided, separated by a space.");
            }
        }
        return new Plateau(x, y);
    }
}
