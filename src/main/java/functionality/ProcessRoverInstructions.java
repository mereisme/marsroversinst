package main.java.functionality;

import main.java.domain.Plateau;
import main.java.domain.Rover;

import java.util.ArrayList;

public class ProcessRoverInstructions {
    public static void processInstructions(Plateau plateau, ArrayList<Rover> rovers) {
        for(Rover r : rovers) {
            for(Character instruction : r.getInstructions()) {
                if(instruction == 'M') { // move
                    // switch dir, N y+1, S y-1, E x+1, W x-1 while not greater than upper bounds, or less than 0, 0
                    switch(r.getDir()) {
                        case 'N':
                            r.setYLoc(r.getYLoc() < plateau.getUpperYCoord() ? r.getYLoc() + 1 : r.getYLoc());
                            break;
                        case 'S':
                            r.setYLoc(r.getYLoc() > 0 ? r.getYLoc() - 1 : r.getYLoc());
                            break;
                        case 'E':
                            r.setXLoc(r.getXLoc() < plateau.getUpperXCoord() ? r.getXLoc() + 1 : r.getXLoc());
                            break;
                        case 'W':
                            r.setXLoc(r.getXLoc() > 0 ? r.getXLoc() - 1 : r.getXLoc());
                            break;
                    }
                } else { // change directions
                    // switch dir, N L ? W : E, S L ? E : W, E L ? N : S, W L ? S : N
                    switch(r.getDir()) {
                        case 'N':
                            r.setDir(instruction == 'L' ? 'W' : 'E');
                            break;
                        case 'S':
                            r.setDir(instruction == 'L' ? 'E' : 'W');
                            break;
                        case 'E':
                            r.setDir(instruction == 'L' ? 'N' : 'S');
                            break;
                        case 'W':
                            r.setDir(instruction == 'L' ? 'S' : 'N');
                            break;
                    }
                }
            }
        }
    }
}
